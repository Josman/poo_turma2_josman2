
public class EntradaDoCinema {
	private String tituloDoFilme;
	private String horario;
	private String sala;
	private String poltrona;
	private double valorOriginal;
	private boolean disponivel;
	
	
	public EntradaDoCinema(String titulo, String horario, String sala, String poltrona, double valor, boolean disponivel){
		this.tituloDoFilme=titulo;
		this.horario=horario;
		this.sala=sala;
		this.poltrona=poltrona;
		this.valorOriginal=valor;
		this.disponivel=disponivel;
	}
	public void calcularValorDesconto(int ano,int dia, int mes) {
		double valor=0;
		if((2018-ano)<=11){
			valor= this.valorOriginal/2;
		}else if((2018-ano)==12){
			if(9-mes>0) {
				valor= this.valorOriginal/2;
			}else if((9-mes)==0){
				if(25-dia>0) {
					valor= this.valorOriginal/2;
				}else {
					valor=this.valorOriginal;
				}
			}
		}
		System.out.println(valor);
	}
	public void calcularValorDesconto(int ano, int mes, int dia,int carteira) {
		double valor=0;
		// caso tenha menos de 12
		if((2018-ano)<=11){
			valor= this.valorOriginal/2;
			
		}else if((2018-ano)==12){
			if(9-mes>0) {
				valor= this.valorOriginal/2;
			}else if((9-mes)==0){
				if(25-dia>0) {
					valor= this.valorOriginal/2;
				}else {
					valor=this.valorOriginal;
				}
			}
		}
		
		//caso tenha entre 12 e 15
		if((2018-ano)>=12 && (2018-ano)<=15){
			valor= this.valorOriginal*0.4;
		}else if((2018-ano)==16){
			if(9-mes>0) {
				valor= this.valorOriginal*0.4;
			}else if((9-mes)==0){
				if(25-dia>0) {
					valor= this.valorOriginal*0.4;
				}else {
					valor=this.valorOriginal;
				}
			}
		}
		// caso tenha entre 16 e 20
		else if((2018-ano)>=16 && (2018-ano)<=20){
			valor= this.valorOriginal*0.3;
		}else if((2018-ano)==21){
			if(9-mes>0) {
				valor= this.valorOriginal*0.3;
			}else if((9-mes)==0){
				if(25-dia>0) {
					valor= this.valorOriginal*0.3;
				}else {
					valor=this.valorOriginal;
				}
			}
		}
		// caso tenha mais de 20
		else if((2018-ano)>21)
			 valor= this.valorOriginal*0.2;
		else valor=this.valorOriginal;
		System.out.println(valor);
		
	}
	public void realizarVenda() {
		this.disponivel=false;
	}
	public String toString() {
		String	toStr="";
		toStr+="Titulo do Filme:"+this.tituloDoFilme+"\n";
		toStr+="Horario:"+this.horario+"\n";
		toStr+="Poltrona:"+this.poltrona+"\n";
		toStr+="Valor:"+this.valorOriginal+"\n";
		 toStr+="Disponibilidade:"+this.disponivel;
		 return toStr;
	}
	public String getTituloDoFilme() {
		return tituloDoFilme;
	}
	public void setTituloDoFilme(String titulo) {
		titulo=this.tituloDoFilme;
	}
	public String getHorario() {
		return horario;
		
	}
	public void setHorario(String horario) {
		horario=this.horario;
	}
	public String getSala() {
		return sala;
	}
	public void setSala(String sala) {
		sala=this.sala;
	}
	public String getPoltrona() {

		return poltrona;
	}
	public void setPoltrona(String poltrona) {
		poltrona=this.poltrona;
	}
	public double getValorOriginal(){
		return valorOriginal;
		
	}
	public void setValorOriginal(double valor) {
		valor=this.valorOriginal;
	}
	public boolean getDisponivel() {
		return disponivel;
	}
	public void setDisponivel(boolean disponivel) {
		disponivel=this.disponivel;
	}
}