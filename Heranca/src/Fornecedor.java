
public class Fornecedor extends Pessoa{
	private double valorCredito;
	private double valorDivida;
	
	public double getValorC() {
		return valorCredito;
	}
	public void setValorC(double valorCredito) {
		this.valorCredito=valorCredito;
	}
	public double getValorD() {
		return valorDivida;
	}
	public void setValorD(double valorD) {
		this.valorDivida=valorD;
	}
	
	public double obterSaldo() {
		return valorCredito-valorDivida;
	}
}
