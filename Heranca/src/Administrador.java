
public class Administrador extends Empregado{
	private double ajudaDeCusto;
	
	public double getAjudDeCusto() {
		return ajudaDeCusto;
	}
	public void setAjudDeCusto(double ajudaCusto) {
		this.ajudaDeCusto=ajudaCusto;
	}
	public double calcularADM() {
		return calcularSalario()+ajudaDeCusto;
	}
	
}
