package ufc.br.poo.josman;

public class ContaCorrente extends ContaBancaria implements Imprimível{
	private double limite;
	
	public ContaCorrente() {
		
	}
	public ContaCorrente(double conta,double saldo,double limite) {
		super(conta,saldo);
		this.limite=limite;
		this.setVariante(2);
	}
	
	public  boolean sacar(double valor) {
		
		if(valor<super.getSaldo()+limite) {
			super.setSaldo(super.getSaldo()-valor);
			return true;
		}else {
			System.out.println("Saldo indisponivel");
			return false;
		}
	}
	
	public boolean depositar(double valor) {
		 this.setSaldo(super.getSaldo()+valor);
		 return true;
	}
	
	@Override
	public String toString() {
		return "ContaCorrente [saldo ="  + getSaldo() + ",conta = " + getConta()  + " limite= " + limite + "] \n "   ;
	}
	@Override
	public void mostrarDados() {
		System.out.println(toString());	
	}
	
	
}