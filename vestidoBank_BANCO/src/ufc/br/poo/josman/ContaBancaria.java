package ufc.br.poo.josman;

public abstract class ContaBancaria {
	private double saldo;
	private double conta;
	private double variante;
	public ContaBancaria() {
		
	}
	public ContaBancaria(double conta,double saldo) {
		this.saldo = saldo;
		this.conta = conta;
	}
	
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public double getConta() {
		return conta;
	}
	public void setConta(double conta) {
		this.conta = conta;
	}
	
	public void transferir(ContaBancaria conta1, ContaBancaria conta2, double valor) {
		
		conta1.sacar(valor);
		conta2.depositar(valor);
		
	}
	
	public abstract boolean sacar(double valor);
	public abstract boolean depositar(double valor);

	
	
	@Override
	public String toString() {
		return "ContaBancaria [saldo=" + saldo + ", conta=" + conta + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(conta);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(saldo);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContaBancaria other = (ContaBancaria) obj;
		if (Double.doubleToLongBits(conta) != Double.doubleToLongBits(other.conta))
			return false;
		if (Double.doubleToLongBits(saldo) != Double.doubleToLongBits(other.saldo))
			return false;
		return true;
	}
	public void mostrarDados() {
		System.out.println(toString());
	}
	public double getVariante() {
		return variante;
	}
	public void setVariante(double variante) {
		this.variante = variante;
	}
	
}
