package ufc.br.poo.josman;

import java.util.ArrayList;
import java.util.List;

public class Banco implements Imprimível{
	private List<ContaBancaria> lista_cb;
	
	public Banco(){
	lista_cb= new ArrayList<ContaBancaria>();
	}
	
	
	public List<ContaBancaria> getLista_cb() {
		return lista_cb;
	}

	public boolean inserir(ContaBancaria conta){
		
		for(int i=0; i < lista_cb.size(); i++) {
			if(lista_cb.get(i).equals(conta)) {
				return false;
			}
		}
		lista_cb.add(conta);
		
		return true;
		
	}
	public void remover(ContaBancaria conta) {
		int indice = procurar(conta.getConta());
		if(indice!=-1) {
		lista_cb.remove(lista_cb.get(indice));
		
		}
	}
	public void removerDigito(ContaBancaria conta,int variante) {
		ContaBancaria indice = procurarDigito(conta.getConta(),variante);
		if(indice!=null) {
		lista_cb.remove(indice);
		
		}
	}
	

	public int procurar(double conta) {
		for(int i=0; i < lista_cb.size(); i++) {
			if(lista_cb.get(i).getConta()==conta ) {
				return i;
				
			}
		}
		return -1;
	}
	
	public ContaBancaria procurarconta(double conta) {
		for(int i=0; i < lista_cb.size(); i++) {
			if(lista_cb.get(i).getConta()==conta ) {
				return lista_cb.get(i);
				
			}
		}
		return null;
	}
	
	public ContaBancaria procurarDigito(double conta, int variante) {
		for(int i=0; i < lista_cb.size(); i++) {
			if(lista_cb.get(i).getConta()==conta && lista_cb.get(i).getVariante()==variante) {
				return lista_cb.get(i);
			}
		}
		return null;
	}


	@Override
	public void mostrarDados() {
		System.out.println(lista_cb.toString());
	}

	
	
}
