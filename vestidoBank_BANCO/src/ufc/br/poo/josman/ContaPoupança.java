package ufc.br.poo.josman;

public class ContaPoupança extends ContaBancaria implements Imprimível{
	private double taxa;
//	private double saldo;
	//private double conta;

	public ContaPoupança() {
		
	}
	public ContaPoupança(double conta,double saldo,double taxa) {
		super(conta,saldo);
		this.taxa = taxa;
		this.setVariante(1);
				
	}
	
	public double getTaxa() {
		return taxa;
	}

	public void setTaxa(double taxa) {
		this.taxa = taxa;
	}
	

	public boolean sacar(double valor) {
		if(valor+taxa<super.getSaldo()) {
			super.setSaldo(super.getSaldo()-valor-taxa);
			return true;

		}else {
			System.out.println("Saldo indisponivel");
			return false;
		}
		
		
	}
	public boolean depositar(double valor) {
		
		super.setSaldo(super.getSaldo()+valor-taxa);
		  return true;
		 
	}

	@Override
	public String toString() {
		return  "ContaPoupança [saldo ="  + getSaldo() + ",conta = " + getConta()  + " taxa= " + taxa + "] \n "   ;
	}
	
	@Override
	public void mostrarDados() {
		System.out.println(toString());
	}
	
}