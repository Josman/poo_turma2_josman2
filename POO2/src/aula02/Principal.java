package aula02;

public class Principal {

	public static void main(String[] args) {
	
		MotoTaxi taxista1= new MotoTaxi("GUANABARA","alalalala","AMO-EBA",9.5F);
		MotoTaxi taxista2= new MotoTaxi("FRETCAR","semcapacete","NEM-UMA",8.4F);
		
		Cliente estudante=new Cliente("GABRIEL","040.002.892.22","Estudante");
		Cliente Idoso=new Cliente("BRUNO","040.002.892.22","Idoso");
		Cliente Professor=new Cliente("TECIO","040.002.892.22","Professor");
		
		Corrida corrida1=new Corrida("quixada","fortaleza",10);
		Corrida corrida2=new Corrida("quixelo","fortim",12);
		Corrida corrida3=new Corrida("Jeri","Maracanau",12);
		
		taxista1.realizarCorrida(estudante,corrida1);
		corrida1.caucularValor(10);
		estudante.solicitarDesconto(corrida1);
		
		System.out.println("\n");
		
		taxista2.realizarCorrida(Idoso, corrida2);
		corrida2.caucularValor(6);
		Idoso.solicitarDesconto(corrida2);
		
		System.out.println("\n");
		
		taxista1.realizarCorrida(Professor, corrida3);
		corrida3.caucularValor(8);
		Professor.solicitarDesconto(corrida3);
	}

}
