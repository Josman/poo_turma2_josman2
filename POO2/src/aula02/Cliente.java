package aula02;

public class Cliente {
	private String nome;
	private String cpf;
	private String tipo;
	
	public Cliente(String nome,String cpf,String tipo) {
		this.nome=nome;
		this.cpf=cpf;
		this.tipo=tipo;
		
		
	}
	
	//GETS E SETS
	public String getNome(){
		return this.nome;
	}
	public void setNome(String nome){
		this.nome=nome;
	}
	public String getCPF(){
		return this.cpf;
	}
	public void setCPF(String cpf){
		this.cpf=cpf;
	}
	public String getTipo(){
		return this.tipo;
	}
	public void setTipo(String tipo){
		this.tipo=tipo;
	}
	
	// GETS E SETS
	
	public float solicitarDesconto(Corrida a){
		float precoCorrida= a.getPrecoCorrida();
		if(getTipo()=="Estudante"){
			precoCorrida=precoCorrida-precoCorrida*(0.5f);
		}else if(getTipo()=="Idoso"){
			precoCorrida=precoCorrida-precoCorrida*(0.5f);
		}else if(getTipo()=="Policial"){
			precoCorrida=precoCorrida-(precoCorrida*(0.2f));
		}else if(getTipo()=="Professor"){
			precoCorrida=precoCorrida-(precoCorrida*(0.1f));
		}
		
		System.out.println(precoCorrida);
		return precoCorrida;
	}
	
}
