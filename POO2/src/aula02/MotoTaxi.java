package aula02;

public class MotoTaxi {
	private String nome;
	private String cnh;
	private String placa;
	private float nota;
	
	public MotoTaxi(String nome, String cnh, String placa, float nota) {
		this.nome=nome;
		this.cnh=cnh;
		this.placa=placa;
		this.nota=nota;
	}

	// GETS E SETS
	public String getNome(){	
		return this.nome;
	}
	public void setNome(String nome){
		this.nome=nome;
	}
	public String getCNH(){	
		return this.cnh;
	}
	public void setCNH(String cnh){
		this.cnh=cnh;
	}
	public String getPlaca(){	
		return this.placa;
	}
	public void setPlaca(String placa){
		this.placa=placa;
	}
	public float getNota(){	
		return this.nota;
	}
	public void setNome(float nota){
		this.nota=nota;
	}
	
	// GETS E SETS
public void realizarCorrida(Cliente a, Corrida x){
		System.out.println(a.getNome());
		System.out.println(getNome());
		System.out.println(getNota());
		System.out.println(x.getPartida());
		System.out.println(x.getDestino());
		
	}
}
