package aula02;

public class Corrida {
	private String partida;
	private float precokm;
	private float precoCorrida;
	private String destino;
	
	public Corrida(String partida,String destino, float precokm){
		this.partida=partida;
		this.destino=destino;
		this.precokm=precokm;
	}
	
	// GETS E SETS
	public String getPartida(){
		return this.partida;
	}
	public void setPartida(String partida){
		this.partida=partida;
	}
	public float getPrecokm(){
		return this.precokm;
	}
	public void setPrecokm(float precokm){
		this.precokm=precokm;
	}
	public float getPrecoCorrida(){
		return this.precoCorrida;
	}
	public void setPrecoCorrida(float precoCorrida){
		this.precoCorrida=precoCorrida;
	}
	public String getDestino(){
		return this.destino;
	}
	public void setDestino(String destino){
		this.destino=destino;
	}
	// GETS E SETS
	
	public float caucularValor(int a){
		this.precoCorrida=a*precokm+5;
		return precoCorrida;
	}
}
